<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>chat</title>
</head>
<body>
<?php
session_start();
include "functions_bank.php";
if(isset($_POST["chatName"])){
	$_SESSION['chatName'] = $_POST["chatName"];
}

if(isset($_POST['replyText'])){
	$replyText = $_SESSION['chatName'].": ".$_POST['replyText']."\n";
	file_put_contents("chatHistory.txt",$replyText,FILE_APPEND);
}

if(!isset($_SESSION['chatName'])){
	inputChatName();
} else{
	echo "<a href='logoff.php'>Log Out</a> <br/>";
	showChatHistoryBox();
	inputChatReply();
}

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(
   function(){
		setInterval(function(){
			$("#chatBox").load("chatHistory.txt");
			if(document.getElementById("chkautoreload").checked){
			var textarea = document.getElementById('chatBox');
			textarea.scrollTop = textarea.scrollHeight;
			}
		},1000);
		}	
	);
	var textarea = document.getElementById('chatBox');
	textarea.scrollTop = textarea.scrollHeight;
</script>
</body>
</html>